package com.imaginaformacion.tema4ej2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().setTitle("Imagina Formación");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //TODO Crea un MyRecyclerAdapter para ello dispones de:
        //ArrayList<Curso> que devuelve la función: DataManager.getInstance().getData(this)
        //El layout de cada tarjeta que es: R.layout.card

        //TODO Establece el layoutManager para que se muestr un grid de dos columnas:
        recyclerView.setAdapter(new MyRecyclerAdapter(this,DataManager.getInstance().getData(this),R.layout.card));
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
    }
}
