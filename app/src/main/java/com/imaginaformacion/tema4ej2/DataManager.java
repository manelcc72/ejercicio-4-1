package com.imaginaformacion.tema4ej2;


import android.content.Context;

import java.util.ArrayList;

public class DataManager {

    private static DataManager dataManager;
    private ArrayList<Curso> cursos;

    public static DataManager getInstance(){
        if(dataManager == null)
            dataManager = new DataManager();
        return dataManager;
    }

    public ArrayList<Curso> getData(Context context){
        if(cursos == null){
            cursos = new ArrayList<Curso>();
            cursos.add(new Curso(context.getString(R.string.curso1title), context.getString(R.string.curso1description), context.getDrawable(R.drawable.curso1drawable),context.getString(R.string.curso1url)));
            cursos.add(new Curso(context.getString(R.string.curso2title), context.getString(R.string.curso2description), context.getDrawable(R.drawable.curso2drawable),context.getString(R.string.curso2url)));
            cursos.add(new Curso(context.getString(R.string.curso3title), context.getString(R.string.curso3description), context.getDrawable(R.drawable.curso3drawable),context.getString(R.string.curso3url)));
            cursos.add(new Curso(context.getString(R.string.curso4title), context.getString(R.string.curso4description), context.getDrawable(R.drawable.curso4drawable),context.getString(R.string.curso4url)));
            cursos.add(new Curso(context.getString(R.string.curso5title), context.getString(R.string.curso5description), context.getDrawable(R.drawable.curso5drawable),context.getString(R.string.curso5url)));
            cursos.add(new Curso(context.getString(R.string.curso6title), context.getString(R.string.curso6description), context.getDrawable(R.drawable.curso6drawable),context.getString(R.string.curso6url)));

        }
        return cursos;

    }
}
